# -*- coding: utf-8 -*-
"""
Created on Wed Jun 14 23:11:56 2017

@author: Jonathan
"""

#Modules! THEY ARE PYTHON FILES
#COLLECTIONS OF THEM!
#SO MANY
#WE CAN IMPORT THEM LIKE WE IMPORT ANY IMPORTABLES! LIKE RANDOM
#WE JUST HAVE TO ASK NICELY
#WE CAN ALSO IMPORT FROM! WHICH WILL ALLOW US TO CALL THINGS BY NAMES
#WITHOUT USING CLUNKY THING.THINGINTHING NOTATIONS!
#FILES HANDLES
round(4.555555,4)

def polysum(n,s):
    '''
    Objective:
        This function will provide the sum of the squared perimeter
        and area of a regular polygon with n sides of length s.
    arguments:
        n=number of sides on the polygon
        s=length of each side
    Definitions Used:
        Polygon Area = (.25*n*s**2)/tan(pi/n)
            tangent outputs in radians by default
        Polygon Perimeter = n*s
        pi as imported from Python's math module
    '''
    import math
    pi=math.pi
    Area = (0.25*n*s**2)/math.tan(pi/n)
    Perimeter = n*s
    return round(Area+Perimeter**2,4)
    