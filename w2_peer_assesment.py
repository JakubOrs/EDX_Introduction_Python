# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 16:23:01 2017

@author: unknown
"""

from math import *
#imports mathematical functions from math library

def area(n,s):
    '''
    takes in number of sides n and side lengths s
    
    returns area of polygon
    
    '''
    return (0.25*n*(s**2))/(tan(pi/n))

def perimeter(n,s):
    '''
    takes in number of sides n and side lengths s
    
    returns perimeter of polygon
    
    '''
    return n*s

def polysum (n,s):
    '''
    takes in number of sides n and side lengths s
    
    returns the sum of the area and square of the perimeter of the polygon
    
    '''
    answer = area(n,s)+(perimeter(n,s))**2
    result = round(answer, 4) #rounds the float to 4 decimal places
    return float(result)



print(polysum(2,2))
print(polysum(13,18))
print(polysum(10,100))
print(polysum(20,15.5))
#print(polysum(0,3))
print(polysum(3,0))