# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 16:52:35 2017

@author: ASUS
"""

import unittest

balance = 3329
annualInterestRate = 0.2

def LowestFixedPayment (balance, annualInterestRate):
    newBalance = balance
    fixedPayment = 0
    
    while newBalance > 0:
        newBalance = balance
        fixedPayment += 10

        for month in range(1,13):
            newBalance -= fixedPayment
            newBalance += newBalance * annualInterestRate / 12

    return fixedPayment

print("Lowest Payment: ", LowestFixedPayment (balance, annualInterestRate))


class LowestFixedPayment_tests (unittest.TestCase):

    def test_one(self):
        self.assertEqual(310,LowestFixedPayment(3329,0.2))
        
    def test_two(self):
        self.assertEqual(440,LowestFixedPayment(4773,0.2))

    def test_three(self):
        self.assertEqual(360,LowestFixedPayment(3926,0.2))

if __name__=='__main__':
    unittest.main()