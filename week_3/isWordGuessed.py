def isWordGuessed(secretWord, lettersGuessed):

    # Sprawdzam czy argumenty są nullowe
    try:
        if secretWord is None or lettersGuessed is None:
            raise ValueError
    except ValueError:
        return False

    # Sprawdzam czy można iterować po wyrazie
    try:
        _ = iter(secretWord)
    except TypeError:
        return False

    # Dla każdej litery z wyrazu sprawdzam czy jest ona zawarta w liście sprawdzonych liter
    for letter in secretWord:
        if not letter.lower() in [x.lower() for x in lettersGuessed]:
            return False
    return True


