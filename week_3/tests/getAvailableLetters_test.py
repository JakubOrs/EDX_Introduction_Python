import unittest
from getAvailableLetters import getAvailableLetters


class getAvailableLetters_test(unittest.TestCase):
    def _test_apple_full(self):
        self.assertEqual(getAvailableLetters(['a','p','l','e']),'bcdfghijkmnoqrstuvwxyz')

    def test_from_edx(self):
        self.assertEqual(getAvailableLetters(['e', 'i', 'k', 'p', 'r', 's']),'abcdfghjlmnoqtuvwxyz')

    def test_all(self):
        self.assertEqual(getAvailableLetters([]),'abcdefghijklmnopqrstuvwxyz')

if __name__ == '__main__':
    unittest.main()