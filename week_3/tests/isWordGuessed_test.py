import unittest
from isWordGuessed import isWordGuessed

class isWordGuessed_test(unittest.TestCase):
    def test_apple_true(self):
        self.assertTrue(isWordGuessed('apple',['a','p','l','e']))

    def test_apple_false(self):
        self.assertFalse(isWordGuessed('apple',['a','p','l']))

    def test_word_none(self):
        self.assertFalse(isWordGuessed(None,['a']))

    def test_list_empty(self):
        self.assertFalse(isWordGuessed('apple',[]))

    def test_number(self):
        self.assertFalse(isWordGuessed(4,['a','b']))

    def test_two_words(self):
        self.assertFalse(isWordGuessed('apple','pear'))

    def test_two_apples(self):
        self.assertTrue(isWordGuessed('apple','apple'))

    def test_apple_none(self):
        self.assertFalse(isWordGuessed('apple',None))

    def test_double_int(self):
        self.assertFalse(isWordGuessed(7,7))

    def test_apple_tuple(self):
        self.assertTrue(isWordGuessed('apple',{'a','p','l','e'}))

if __name__ == '__main__':
    unittest.main()