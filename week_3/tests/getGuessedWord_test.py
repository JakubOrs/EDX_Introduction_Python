import unittest
from getGuessedWord import getGuessedWord


class getGuessedWord_test(unittest.TestCase):
    def test_apple_full(self):
        self.assertEqual(getGuessedWord('apple',['a','p','l','e']),'apple')

    def test_apple_missing(self):
        self.assertEqual(getGuessedWord('appl_',['a','p','l']),'appl_')


if __name__ == '__main__':
    unittest.main()