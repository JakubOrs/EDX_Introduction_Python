def getGuessedWord(secretWord, lettersGuessed):

    # Sprawdzam czy argumenty są nullowe
    try:
        if secretWord is None or lettersGuessed is None:
            raise ValueError
    except ValueError:
        return False

    # Sprawdzam czy można iterować po wyrazie
    try:
        _ = iter(secretWord)
    except TypeError:
        return False

    output = ''

    # Dla każdej litery z wyrazu sprawdzam czy jest ona zawarta w liście sprawdzonych liter
    for letter in secretWord:
        # Jeśli jest to dodaję ją do wyniku
        if letter.lower() in [x.lower() for x in lettersGuessed]:
            output += letter.lower()
        # Jeśli jej nie ma to dodaję '_'
        else:
            output += '_'

    return output
    # " ".join(output)


