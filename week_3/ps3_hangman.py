# Hangman game
#

# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)

from isWordGuessed import isWordGuessed
from getAvailableLetters import getAvailableLetters
from getGuessedWord import getGuessedWord

import random
import string

WORDLIST_FILENAME = "data/words.txt"


def loadWords():
    """
    Returns a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist


def chooseWord(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)


# end of helper code
# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = loadWords()

secretWord = chooseWord(wordlist)

print(secretWord)




def hangman(secretWord):
    '''
    secretWord: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many
      letters the secretWord contains.

    * Ask the user to supply one guess (i.e. letter) per round.

    * The user should receive feedback immediately after each guess
      about whether their guess appears in the computers word.

    * After each round, you should also display to the user the
      partially guessed word so far, as well as letters that the
      user has not yet guessed.

    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE...

    # Sprawdzam czy argument jest nullowe
    try:
        if secretWord is None:
            raise ValueError
    except ValueError:
        return False

    # Sprawdzam czy argument jest stringiem
    try:
        if not isinstance(secretWord,str):
            raise ValueError
    except ValueError:
        return False


    # Inicjuję początkowe wartości zmiennych
    leftGuesses = 8
    mistakesMade = 0
    lettersGuessed = list()
    availableLetters = list(string.ascii_lowercase)

    print("Welcome to the game Hangman!")
    # TODO Dodać funkcjonalność zmiany 'letters' na 'letter' dla 1 literowców
    print("I am thinking of a word that is",len(secretWord),"letters long")
    print("-----------")

    while leftGuesses > 0 and secretWord != getGuessedWord(secretWord,lettersGuessed):
        print("You have", leftGuesses, "guesses left")
        print("Available letters:",''.join(availableLetters))
        inputLetter = None
        while not inputLetter or len(inputLetter) > 1 or inputLetter not in string.ascii_letters:
            inputLetter = str(input("Please guess a letter: "))
            inputLetter = inputLetter.lower()
            if inputLetter in lettersGuessed:
                print("Oops! You've already guessed that letter:",getGuessedWord(secretWord, lettersGuessed))
                mistakesMade += 1
            else:
                lettersGuessed.append(inputLetter)
                availableLetters.remove(inputLetter)
                if inputLetter not in secretWord:
                    print("Oops! That letter is not in my word: _")
                    leftGuesses -= 1
                    mistakesMade += 1
                else:
                    print("Good guess:"," ".join(getGuessedWord(secretWord, lettersGuessed)))

        print("-----------")
    if secretWord == getGuessedWord(secretWord,lettersGuessed):
        print("Congratulations, you won!")
    elif leftGuesses == 0:
        print("Sorry, you ran out of guesses.")
        print("The word was:",secretWord)


# When you've completed your hangman function, uncomment these two lines
# and run this file to test! (hint: you might want to pick your own
# secretWord while you're testing)

# secretWord = chooseWord(wordlist).lower()
hangman(secretWord)
