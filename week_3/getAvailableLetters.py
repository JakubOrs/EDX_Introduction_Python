import string

def getAvailableLetters(lettersGuessed):

    # Sprawdzam czy argumenty są nullowe
    try:
        if lettersGuessed is None:
            raise ValueError
    except ValueError:
        return False

    output = ''

    # Dla każdej litery z alfabetu wyrazu sprawdzam czy jest ona zawarta w liście sprawdzonych liter
    for letter in list(string.ascii_lowercase):
        # Jeśli nie ma jej to dodaję ją do wyniku
        if letter.lower() not in [x.lower() for x in lettersGuessed]:
            output += letter.lower()

    return output


