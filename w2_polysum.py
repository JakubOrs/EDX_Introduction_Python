'''
Regular Polygons: polysum

A regular polygon has 'n' number of sides. Each side has length 's'.

* The area of regular polygon is: (0.25*n*s^2)/tan(pi/n)
* The perimeter of a polygon is: length of the boundary of the polygon

Write a function called 'polysum' that takes 2 arguments, 'n' and 's'.
This function should sum the area and square of the perimeter of the regular polygon.
The function returns the sum, rounded to 4 decimal places.

+++ IMPORTANT NOTE +++
You must upload a .py file.
Any code you enter in the box will have its spacing removed,
so will be unreadable by your peers.
In the box type in anything, for example, "attached".
'''

# n number of sides
# s length of a single side

def PolysumArea (n,s):
    from math import pow, tan, pi

    # checking if n can be converted to int and s can be converted to float
    try:
        int(n) and float(s)
    except ValueError:
        return False

    area = (0.25 * n * pow(s,2)) / tan(pi/n)

    return area

def PolysumPerimeter(n,s):
    # checking if n can be converted to int and s can be converted to float
    try:
        int(n) and float(s)
    except ValueError:
        return False

    perimeter = n * s

    return perimeter

def Polysum(n,s):
    from math import pow

    # checking if n can be converted to int and s can be converted to float
    try:
        float(n) and float(s)
    except ValueError:
        return False

    area = PolysumArea(n,s)
    perimeter = PolysumPerimeter(n,s)
    
    polysum = area + pow(perimeter,2)

    return round(polysum,4)

'''
I prefer to have my functions starting with capital letter
but the requirement was to have it starting with lower letter
'''
def polysum(n,s):
    return Polysum(n,s)

print(polysum(2,2))
print(polysum(13,18))
print(polysum(10,100))
print(polysum(20,15.5))
#print(polysum(0,3))
print(polysum(3,0))