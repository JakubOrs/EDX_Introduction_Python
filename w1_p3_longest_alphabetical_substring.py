def longest_substring(s):
    words=list()
    #print(len(s))
    
    if not len(s):
        return ''
    else:
        i=0
        while i<len(s):
            j=1
            while i+j<len(s):
                #print("%d  %d %s %s" %(i, i+j-1, s[i+j-1], s[i+j]))
                if s[i+j-1]>s[i+j]:
                    #print("output")
                    words.append(s[i:i+j])
                    i+=j-1
                    break
                elif i+j==len(s)-1:
                    #print("output")
                    words.append(s[i:i+j+1])
                    i+=j-1
                    break
                j+=1
            i+=1
                    
        words.sort(key=lambda s: -len(s))
        return words[0]

