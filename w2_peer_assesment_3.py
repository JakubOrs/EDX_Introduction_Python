# -*- coding: utf-8 -*-
"""
Created on Wed Jun 14 18:58:41 2017

@author: yclo
"""

def polysum(n,s):
    '''
    n: number of sides
    s: side length
    
    returns: area + perimeter**2 rounded to 4 decimal places
    '''
    
    import math
    
    area = 0.25*n*s**2/math.tan(math.pi/n)
    perimeter = n*s
    return round(area+perimeter**2,4)
