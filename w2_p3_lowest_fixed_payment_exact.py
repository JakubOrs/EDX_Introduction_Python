# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 17:47:09 2017

@author: ASUS
"""

import unittest

# balance = 3329
# annualInterestRate = 0.2

def LowestFixedPaymentExact (balance, annualInterestRate):

    monthlyInterestRate = annualInterestRate / 12
    lowerBound = balance / 12.0
    upperBound = (balance * (1 + monthlyInterestRate) ** 12) / 12.0

    newBalance = balance
    fixedPayment = (lowerBound + upperBound) / 2.0
    iteration = 0

    while abs(newBalance) > 0.01  and iteration < 1000:
        if iteration > 0:
            if newBalance < 0:
                upperBound = fixedPayment
            else:
                lowerBound = fixedPayment

            fixedPayment = (lowerBound + upperBound) / 2.0

        newBalance = balance

        for month in range(1,13):
            newBalance -= fixedPayment
            newBalance += newBalance * monthlyInterestRate

        iteration +=1

    return round(fixedPayment,2)

print("Lowest Payment: ", LowestFixedPaymentExact (balance, annualInterestRate))


class LowestFixedPayment_tests (unittest.TestCase):

    def test_one(self):
        self.assertEqual(29157.09,LowestFixedPaymentExact(320000,0.2))
        
    def test_two(self):
        self.assertEqual(90325.03,LowestFixedPaymentExact(999999,0.18))

if __name__=='__main__':
    unittest.main()
