# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 16:32:43 2017

@author: ASUS
"""

import unittest


def DebtAfterOneYear (balance, annualInterestRate, monthlyPaymentRate):
    remainingBalance = balance

    for month in range(1,13):
        remainingBalance -= round(remainingBalance * monthlyPaymentRate, 2)
        interest = round(remainingBalance * annualInterestRate / 12,2)
        remainingBalance += interest

    return round(remainingBalance,2)

#print("Remaining balance:", DebtAfterOneYear (balance, annualInterestRate, monthlyPaymentRate))
#print(DebtAfterOneYear(42,0.2,0.04))
#print(DebtAfterOneYear(484,0.2,0.04))

class DebtAfterOneYear_tests (unittest.TestCase):

    def test_one(self):
        self.assertEqual(31.38,DebtAfterOneYear(42,0.2,0.04))
        
    def test_two(self):
        self.assertEqual(361.61,DebtAfterOneYear(484,0.2,0.04))

if __name__=='__main__':
    unittest.main()