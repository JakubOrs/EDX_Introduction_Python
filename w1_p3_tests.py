import unittest
from w1_p3_longest_alphabetical_substring import longest_substring

class longest_substring_tests(unittest.TestCase):

	def test_cxivtzgsemi(self):
		self.assertTrue('cx'==longest_substring('cxivtzgsemi'))

	def test_cujcrymyu(self):
		self.assertTrue('cry'==longest_substring('cujcrymyu'))

	def test_utvckjjicfpllmejvqcl(self):
		self.assertTrue('cfp'==longest_substring('utvckjjicfpllmejvqcl'))

	def test_ddldmdpbzxl(self):
		self.assertTrue('ddl'==longest_substring('ddldmdpbzxl'))

	def test_jtrulfnccca(self):
		self.assertTrue('ccc'==longest_substring('jtrulfnccca'))

	def test_bhpjbxqvpst(self):
		self.assertTrue('bhp'==longest_substring('bhpjbxqvpst'))

	def test_bzedxumegot(self):
		self.assertTrue('egot'==longest_substring('bzedxumegot'))

	def test_abcdefghijklmnopqrstuvwxyz(self):
		self.assertTrue('abcdefghijklmnopqrstuvwxyz'==longest_substring('abcdefghijklmnopqrstuvwxyz'))

	def test_evrhekjxyjuhcjny(self):
		self.assertTrue('cjny'==longest_substring('evrhekjxyjuhcjny'))

	def test_rdrterpersswcciml(self):
		self.assertTrue('erssw'==longest_substring('rdrterpersswcciml'))

	def test_dcba(self):
		self.assertTrue('d'==longest_substring('dcba'))

	def test_null(self):
		self.assertTrue(''==longest_substring(''))

if __name__=='__main__':
	unittest.main()