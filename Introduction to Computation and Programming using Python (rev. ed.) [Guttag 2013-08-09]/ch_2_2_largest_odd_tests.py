import unittest
from ch_2_2_largest_odd import isOdd, largestOdd


class idOdd_tests(unittest.TestCase):

	def test_isOdd_int_odd(self):
		self.assertTrue(isOdd(3))

	def test_isOdd_int_even(self):
		self.assertFalse(isOdd(4))

	def test_isOdd_float_odd(self):
		self.assertTrue(isOdd(11.0))

	def test_isOdd_float_even(self):
		self.assertFalse(isOdd(20.0))

	def test_isOdd_float_float(self):
		self.assertFalse(isOdd(20.3))

	def test_isOdd_float_below_0(self):
		self.assertFalse(isOdd(-12.0))

	def test_isOdd_string(self):
		self.assertFalse(isOdd('test_isOdd'))

	def test_isOdd_string_int_even(self):
		self.assertFalse(isOdd('2'))

	def test_isOdd_string_int_odd(self):
		self.assertFalse(isOdd('3'))

	def test_isOdd_string_float_even(self):
		self.assertFalse(isOdd('2.0'))

	def test_isOdd_string_float_odd(self):
		self.assertFalse(isOdd('3.0'))

	def test_isOdd_string_float_float(self):
		self.assertFalse(isOdd('2.5'))

#   def test_isOdd_null(self):
#       self.assertFalse(isOdd(None))

	def test_isOdd_string_null(self):
		self.assertFalse(isOdd(''))

class largestOdd_tests(unittest.TestCase):

	def test_largestOdd_3(self):
		self.assertTrue(largestOdd(3)==3)

	def test_largestOdd_100_87_31(self):
		self.assertTrue(largestOdd(100,87,31)==87)

	def test_largestOdd_below_odd(self):
		self.assertTrue(largestOdd(-100,-87,-31)==-31)
        
	def test_largestOdd_12_0_11_5(self):
		self.assertTrue(largestOdd(12.0,11.5)=='No odd numbers!')

	def test_largestOdd_strings(self):
		self.assertTrue(largestOdd('2','3.0')=='No odd numbers!')

	def test_largestOdd_20_4_14(self):
		self.assertTrue(largestOdd(20,4.0,14)=='No odd numbers!')

	def test_largestOdd_below_even(self):
		self.assertTrue(largestOdd(-20,-4.0,-14)=='No odd numbers!')
        
        
if __name__ == '__main__':
	unittest.main()