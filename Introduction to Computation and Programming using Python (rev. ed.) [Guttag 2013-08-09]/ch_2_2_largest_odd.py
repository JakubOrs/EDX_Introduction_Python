def isOdd(num):
	try:
		num is not None and int(num)
	except ValueError:
		# print('Not a number!')
		return False

	if int(num)!=num or int(num)%2==0:
		return False
	else:
		return True



def largestOdd(*args):
    inputList = list()
    for num in args:
        if isOdd(num):
            inputList.append(num)
    
    if len(inputList) > 0:
        inputList.sort(reverse=True)
        return inputList[0]
    else:
        return ('No odd numbers!')
    
#print(largestOdd('s',2,0,22))
#print(largestOdd(12.0,11.5))